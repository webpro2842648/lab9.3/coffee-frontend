import { ref, computed, watch } from 'vue'
import { defineStore } from 'pinia'
import type { ReceiptItem } from '@/types/ReceiptItem'
import type { Receipt } from '@/types/Receipt'
import { useAuthStore } from './auth'
import orderService from '@/services/order'
import { useLoadingStore } from './loading'
import { useMessageStore } from './message'
export const useReceiptStore = defineStore('receipt', () => {
  const authStore = useAuthStore()
  const loadingStore = useLoadingStore()
  const messageStore = useMessageStore()
  const receiptItems = ref<ReceiptItem[]>([])
  const receipt = ref<Receipt>()
  initReceipt()
  function initReceipt() {
    receipt.value = {
      id: 0,
      createdDate: new Date(),
      total: 0,
      amount: 0,
      change: 0,
      paymentType: '',
      userId: authStore.getCurrentUser()!.id!,
      memberId: -1,
      user: authStore.getCurrentUser()!
    }
    receiptItems.value = []
  }
  watch(
    receiptItems,
    () => {
      calReceipt()
    },
    { deep: true }
  )
  const calReceipt = function () {
    receipt.value!.total = 0
    receipt.value!.amount = 0
    for (let i = 0; i < receiptItems.value.length; i++) {
      receipt.value!.total += receiptItems.value[i].price * receiptItems.value[i].unit
      receipt.value!.amount += receiptItems.value[i].unit
    }
  }

  const addReceiptItem = (newReceiptItem: ReceiptItem) => {
    receiptItems.value.push(newReceiptItem)
    calReceipt()
  }
  const deleteReceiptItem = (selectedItem: ReceiptItem) => {
    const index = receiptItems.value.findIndex((item) => item === selectedItem)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  const incUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit++
    calReceipt()
  }
  const decUnitOfReceiptItem = (selectedItem: ReceiptItem) => {
    selectedItem.unit--
    if (selectedItem.unit === 0) {
      deleteReceiptItem(selectedItem)
    }
    calReceipt()
  }
  const removeItem = (item: ReceiptItem) => {
    const index = receiptItems.value.findIndex((ri) => ri === item)
    receiptItems.value.splice(index, 1)
    calReceipt()
  }
  const order = async () => {
    try {
      loadingStore.doLoad()
      await orderService.addOrder(receipt.value!, receiptItems.value)
      initReceipt()
      loadingStore.finish()
    } catch (e: any) {
      loadingStore.finish()
      messageStore.showMessage(e.message)
    }
  }
  return {
    receipt,
    receiptItems,
    addReceiptItem,
    incUnitOfReceiptItem,
    decUnitOfReceiptItem,
    deleteReceiptItem,
    removeItem,
    order
  }
})
