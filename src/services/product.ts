import type { Product } from '@/types/Product'
import http from './http'

function addProduct(product: Product & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', product.price.toString())
  formData.append('type', JSON.stringify(product.type))
  if (product.files && product.files.length > 0) formData.append('file', product.files[0])
  return http.post('/products', formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function updateProduct(product: Product & { files: File[] }) {
  const formData = new FormData()
  formData.append('name', product.name)
  formData.append('price', product.price.toString())
  formData.append('type', JSON.stringify(product.type))
  if (product.files && product.files.length > 0) formData.append('file', product.files[0])
  return http.post(`/products/${product.id}`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}

function delProduct(product: Product) {
  return http.delete(`/products/${product.id}`)
}

function getProduct(id: number) {
  return http.get(`/products/${id}`)
}

function getProductsByType(typeId: number) {
  return http.get('/products/type/' + typeId)
}

function getProducts() {
  return http.get('/products')
}

export default { addProduct, updateProduct, delProduct, getProduct, getProducts ,getProductsByType}
